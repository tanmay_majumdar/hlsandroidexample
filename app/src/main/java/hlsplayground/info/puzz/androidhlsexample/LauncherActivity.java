package hlsplayground.info.puzz.androidhlsexample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

public class LauncherActivity extends AppCompatActivity {

    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_launcher);

        editText = findViewById(R.id.etText);

        findViewById(R.id.play).setOnClickListener((v) -> {
            String videoLink = editText.getText().toString();

            if(videoLink == null) {
                Toast.makeText(this, "Please enter a valid link", Toast.LENGTH_LONG).show();
            }

            Intent i = new Intent(this, HLSActivity.class);
            i.putExtra("videoLink", videoLink);
            startActivity(i);
        });
    }
}
